all: build/term

build/term: build/main.o
	g++ -Wall -Wextra -lSDL2 -o $@ $^

build/main.o: src/main.cpp
	g++ -Wall -Wextra -c -lSDL2 -o $@ $^

clean:
	rm build/*

